import numpy as np


ORTHO_DIRECTIONS = [
    [0, 0, 0],
    [1, 0, 0],
    [-1, 0, 0],
    [0, 0, 0],
    [0, 1, 0],
    [0, -1, 0],
    [0, 0, 0],
    [0, 0, 1],
    [0, 0, -1],
]
ORTHO_DIRECTIONS = np.array(ORTHO_DIRECTIONS)


def get_ortho_axis(v: np.array) -> np.array:
    return ORTHO_DIRECTIONS[
        np.argmax(
            np.max(v * ORTHO_DIRECTIONS, axis=1)
        )
    ]
