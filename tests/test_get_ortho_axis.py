import numpy as np
import pytest

from axis_getter import get_ortho_axis


@pytest.mark.parametrize(
    "vec,exp_direction",
    [
        (
            np.array([0.4, 0.5, 0]),
            np.array([0, 1, 0]),
        ),
        (
            np.array([0.6, 0.5, 0]),
            np.array([1, 0, 0]),
        ),
        (
            np.array([0.6, 0.5, 1]),
            np.array([0, 0, 1]),
        ),
        (
            np.array([0.6, 2.5, 1]),
            np.array([0, 1, 0]),
        ),
    ],
)
def test_get_ortho_axis(vec, exp_direction):
    assert (
        exp_direction == get_ortho_axis(vec)
    ).all()
